set search_path to 'management';

CREATE TABLE IF NOT EXISTS role (
        id SERIAL NOT NULL,
        libelle VARCHAR(250) NOT NULL,
        systeme BOOLEAN NOT NULL default false,
        CONSTRAINT pk_role PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS permission (
        code VARCHAR(50) NOT NULL,
        libelle VARCHAR(250) NOT NULL,
        scope VARCHAR(50) NOT NULL,
        ressource VARCHAR(250) NOT NULL,
        contexte VARCHAR(250) NOT NULL,
        action VARCHAR(250) NOT NULL,
        CONSTRAINT pk_permission PRIMARY KEY (code),
        CONSTRAINT unique_permission UNIQUE (scope, ressource, contexte, action)
);

CREATE TABLE IF NOT EXISTS role_permission (
        role_id INTEGER NOT NULL,
        permission_code VARCHAR(50) NOT NULL,
        CONSTRAINT pk_role_permission PRIMARY KEY (role_id, permission_code),
        CONSTRAINT fk_role_permission_role_id FOREIGN KEY (role_id) REFERENCES role (id),
        CONSTRAINT fk_role_permission_permission_code FOREIGN KEY (permission_code) REFERENCES permission (code)
);

CREATE TABLE IF NOT EXISTS utilisateur (
        id SERIAL NOT NULL,
        nom VARCHAR(250) NULL,
        prenom VARCHAR(250) NULL,
        login VARCHAR(250) NOT NULL,
        hash VARCHAR(250) NULL,
        sel VARCHAR(250) NULL,
        nombre_iteration INTEGER NULL,
        email VARCHAR(250) NULL,
        type_authentification VARCHAR(50) NOT NULL default 'LOCALE',
        systeme BOOLEAN NOT NULL default false,
        role_id INTEGER NULL,
        CONSTRAINT pk_utilisateur PRIMARY KEY (id),
        CONSTRAINT unique_utilisateur_email UNIQUE (email),
        CONSTRAINT fk_utilisateur_role_id FOREIGN KEY (role_id) REFERENCES role (id)
);

INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_SUP01', 'Accès à la supervision des flux', 'cassis', 'supervision-flux', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_SUP02', 'Accès à la supervision de la passerelle', 'cassis', 'supervision-passerelle', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_SUP03', 'Accès à la supervision des queues', 'cassis', 'supervision-queues', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_SUP04', 'Accès à la supervision des messages d''une queue', 'cassis', 'supervision-queue-detail', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_PARAM01', 'Accès au paramétrage des partenaires', 'cassis', 'parametrage-partenaires', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_PARAM02', 'Accès au paramétrage des routes', 'cassis', 'parametrage-bindings', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_PARAM03', 'Accès au paramétrage des queues', 'cassis', 'parametrage-queues', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_PARAM04', 'Accès au paramétrage des échanges', 'cassis', 'parametrage-exchanges', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_ADMIN01', 'Accès à l''administration des utilisateurs', 'cassis', 'admnistration-utilisateurs', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_ADMIN02', 'Accès à l''administration des rôles', 'cassis', 'admnistration-roles', '*', 'visualisation');
INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_MONI01', 'Accès au monitoring des microservices', 'cassis', 'monitoring-microservices', '*', 'visualisation');


INSERT INTO role (id, libelle, systeme) VALUES (1, 'administrateur·rice', true);
INSERT INTO role (id, libelle, systeme) VALUES (2, 'superviseur·euse', true);
INSERT INTO role (id, libelle, systeme) VALUES (3, 'paramétreur·ice', true);
INSERT INTO role (id, libelle, systeme) VALUES (4, 'monitoring', true);

INSERT INTO role_permission (SELECT 1, code FROM permission);
INSERT INTO role_permission (SELECT 2, code FROM permission WHERE  code ~ '.*SUP.*');
INSERT INTO role_permission (SELECT 3, code FROM permission WHERE  code ~ '.*PARAM.*');
INSERT INTO role_permission (SELECT 4, code FROM permission WHERE  code ~ '.*MONI.*');

SELECT SETVAL('management.utilisateur_id_seq', COALESCE(MAX(id), 1) ) FROM management.utilisateur;
SELECT SETVAL('management.role_id_seq', COALESCE(MAX(id), 1) ) FROM management.role;

